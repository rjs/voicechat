/*
 * voicechat -- Voice chat via TCP/IP
 * Copyright (C) 1995-1996 Riku Saikkonen
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Author: Riku Saikkonen <rjs@netti.fi>
 */

#include <unistd.h>
#include <string.h>
#include "voicechat.h"

#define SIBUFSZ AUDIOSZ_U*10 /* XXX */
#define NIBUFSZ (AUDIOSZ_C+1)*10 /* XXX */

char sibuf[SIBUFSZ],nibuf[NIBUFSZ];
int sibufpos=0,nibufpos=0,netinsize=0;

/* soundrfd has data for reading, handle it */

void handle_soundin(void)
{
  int i;
  char *bp;
  
  /* Read data */
  i=read(soundrfd,sibuf+sibufpos,SIBUFSZ-sibufpos);
  if (i<=0) return; /* Got none */
  sibufpos+=i;

  /* Process complete packets */
  for (bp=sibuf;sibufpos>AUDIOSZ_U;bp+=AUDIOSZ_U,sibufpos-=AUDIOSZ_U)
  {
    /* We have a packet (size AUDIOSZ_U) in bp */
    /* Compress it and write it to netout */
    send_sound(bp);
  }

  /* Move leftovers to start of sibuf */
  if (sibufpos>0) memmove(sibuf,bp,sibufpos);

  /* We're done */
  return;
}

/* netfd has data for reading, handle it */

void handle_netin(void)
{
  int i,quit;
  char *bp,ch;

  /* Read data */
  i=read(netfd,nibuf+nibufpos,NIBUFSZ-nibufpos);
  if (i<=0) return; /* Got none */
  nibufpos+=i;netinsize+=i;

  /* Process complete packets */
  
  bp=nibuf;quit=0;
  while (nibufpos>0 && !quit)
  {
    switch (*bp)
    {
      case 'C': /* Character */
        if (nibufpos<2) {quit=1;break;} /* Incomplete packet */
        disp_writeother(*(bp+1));
        bp+=2;nibufpos-=2;break;
      case 'A': /* Audio data */
        if (nibufpos<AUDIOSZ_C+1) {quit=1;break;} /* Incomplete packet */
        receive_sound(bp+1);
        bp+=AUDIOSZ_C+1;nibufpos-=AUDIOSZ_C+1;break;
      case 'a': /* Audio silence */
        receive_sound_silence();
        flush_soundout=sobufpos;
        bp++;nibufpos--;break;
      case 'P': /* Audio pause */
        flush_soundout=sobufpos;
        bp++;nibufpos--;break;
      case 'R': /* Recording started */
        orecording=1;update_status=1;
        bp++;nibufpos--;break;
      case 'r': /* Recording stopped */
        orecording=0;update_status=1;
        flush_soundout=sobufpos;
        bp++;nibufpos--;break;
      case 'Q': /* Quitting */
        quitting=1;
        bp++;nibufpos--;break;
      case 'T': /* Test request */
        ch='t';netout(&ch,1); /* Reply */
        bp++;nibufpos--;break;
      case 't': /* Test reply */
        message("Test reply received.");
        bp++;nibufpos--;break;
      default:
        warning("Invalid data from net");
        bp++;nibufpos--;break;
    }
  }

  /* Move leftovers to start of nibuf */
  if (nibufpos>0) memmove(nibuf,bp,nibufpos);

  /* We're done */
  return;
}
