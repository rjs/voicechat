/*
 * voicechat -- Voice chat via TCP/IP
 * Copyright (C) 1995-1996 Riku Saikkonen
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Author: Riku Saikkonen <rjs@netti.fi>
 */

#ifndef VOICECHAT_H

#include "config.h"

/* Banners */

#define SHORTBANNER "voicechat 0.50"

#define BANNER \
  "Voicechat version 0.50, Copyright (C) 1995-1996 Riku Saikkonen\n" \
  "Voicechat comes with ABSOLUTELY NO WARRANTY. This is free software,\n" \
  "and you are welcome to redistribute it under certain conditions;\n" \
  "see the documentation for details.\n"

/* Audio packet sizes (fixed by GSM) */
#define AUDIOSZ_C 33 /* Compressed */
#define AUDIOSZ_U 160 /* Uncompressed */

/* External variables and function prototypes */

/* bufin.c */

extern int netinsize;

void handle_soundin(void);
void handle_netin(void);

/* bufout.c */

extern int nobufpos,sobufpos,flush_soundout,netoutsize;

void netout(char *buf, int len);
void handle_netout(void);
void soundout(char *buf, int len);
void handle_soundout(void);

/* display.c */

extern int display_initialised;
extern int orecording,update_status,testreply;

void disp_init(void);
void disp_tick(void);
void disp_redraw(void);
void disp_deinit(void);
void disp_writeown(char ch);
void disp_writeother(char ch);
void disp_writemsg(char *msg);

/* error.c */

void message(char *msg);
void warning(char *msg);
void fatal(char *msg);
void fatals(char *msg);

/* keyboard.c */

void handle_kbd(void);

/* net.c */

extern int netfd;

void open_net_connect(char *hostname, int port);
void open_net_listen(int port);
void close_net(void);

/* sound.c */

extern int recording,soundfd,soundrfd,silencelimit;

void sound_init(void);
void sound_deinit(void);
void record_on(void);
void record_off(void);
void play_sound(void);
void send_sound(char *sp);
void receive_sound(char *sp);
void receive_sound_silence(void);

/* voicechat.c */

extern int quitting;

#endif /* VOICECHAT_H */
