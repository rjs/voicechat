/*
 * voicechat -- Voice chat via TCP/IP
 * Copyright (C) 1995-1996 Riku Saikkonen
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Author: Riku Saikkonen <rjs@netti.fi>
 */

#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <gsm.h>
#ifdef linux
#include <sys/soundcard.h>
#include <sys/ioctl.h>
#endif
#include "ulaw.h"
#include "voicechat.h"

void send_sound_silence(void);

int recording;
int soundfd=-1,soundrfd=-1;
int silencelimit=DEF_SILENCELIMIT;

gsm gsm_handle;
gsm_frame cbuf;
short sbuf[AUDIOSZ_U];
char ubuf[AUDIOSZ_U];

void sound_init(void)
{
#if (!defined(SOUND_UNIDIR)&&defined(SOUND_ONEDEVICE))
  soundfd=open(SOUND_PLAYDEVICE,O_RDWR|O_NONBLOCK);
  soundrfd=soundfd;
#else
  soundfd=open(SOUND_PLAYDEVICE,O_WRONLY|O_NONBLOCK);
# ifdef SOUND_UNIDIR
  soundrfd=-1;
# else
  soundrfd=open(SOUND_RECDEVICE,O_RDONLY|O_NONBLOCK);
  if (soundrfd<0) fatals("opening audio device");
# endif
#endif
  if (soundfd<0) fatals("opening audio device");

  gsm_handle=gsm_create();
  if (gsm_handle==0) fatal("initialising GSM");

  recording=0;
}

void sound_deinit(void)
{
  if (soundfd>=0) close(soundfd);
  if (soundrfd>=0 && soundrfd!=soundfd) close(soundrfd);

  gsm_destroy(gsm_handle);
}

void record_on(void)
{
#ifdef SOUND_UNIDIR
  close(soundfd);soundfd=-1;
  soundrfd=open(SOUND_RECDEVICE,O_RDONLY|O_NONBLOCK);
  if (soundrfd<0) fatals("opening audio device");
#endif
  recording=1;
}

void record_off(void)
{
#ifdef SOUND_UNIDIR
  close(soundrfd);soundrfd=-1;
  soundfd=open(SOUND_PLAYDEVICE,O_WRONLY|O_NONBLOCK);
  if (soundfd<0) fatals("opening audio device");
#endif
  recording=0;
}

/* Actually play the sound written to /dev/audio */
void play_sound(void)
{
#ifdef linux
  ioctl(soundfd,SNDCTL_DSP_POST);
#endif
}

/* Compress and send a sound packet */
void send_sound(char *sp)
{
  char ch;
  short minv=32700,maxv=-32700;
  char *chp;
  short *shp;
  int i;

  /* Convert from u-law (sp) to 13-bit (sbuf) and get volume */
  for (i=0,chp=sp,shp=sbuf;i<AUDIOSZ_U;i++,chp++,shp++)
  {
    *shp=U2S(*chp);
    if (*shp>maxv) maxv=*shp;
    if (*shp<minv) minv=*shp;
  }

  if (maxv-minv<silencelimit)
  {
    send_sound_silence();
    return;
  }
    
  /* Compress */
  gsm_encode(gsm_handle,(gsm_signal *)sbuf,(gsm_byte *)cbuf);

  /* And send */
  ch='A';netout(&ch,1);
  /* XXX What if netout overruns here? */
  netout((char *)cbuf,AUDIOSZ_C);
}

/* Send a packet of silence */
void send_sound_silence(void)
{
  char ch='a';
  netout(&ch,1);
}

/* Decompress and play a sound packet */
void receive_sound(char *sp)
{
  char *chp;
  short *shp;
  int i;
  
  /* Decompress */
  gsm_decode(gsm_handle,(gsm_byte *)sp,(gsm_signal *)sbuf);

  /* Convert from 13-bit (sbuf) to u-law (ubuf) */
  for (i=0,shp=sbuf,chp=ubuf;i<AUDIOSZ_U;i++,shp++,chp++) *chp=S2U(*shp);

  /* And play */
  soundout(ubuf,AUDIOSZ_U);
}

/* Play a packet of silence */
void receive_sound_silence(void)
{
  memset(ubuf,0,AUDIOSZ_U);
  soundout(ubuf,AUDIOSZ_U);
}
