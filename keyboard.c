/*
 * voicechat -- Voice chat via TCP/IP
 * Copyright (C) 1995-1996 Riku Saikkonen
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Author: Riku Saikkonen <rjs@netti.fi>
 */

#include "voicechat.h"
#ifdef SVR4
#include <curses.h>
#else
#include <ncurses.h>
#endif

/* At least one character in stdin, handle it */
void handle_kbd(void)
{
  int ch;
  char buf[2];

  while ((ch=getch())!=ERR)
  {
    switch (ch)
    {
      case 1: /* ^A: record on/off */
        if (recording)
        {
          record_off();
          *buf='r';netout(buf,1);
        } else
        {
          record_on();
          *buf='R';netout(buf,1);
        }
        update_status=1;
        break;
      case 16: /* ^P: send a pause */
        *buf='P';netout(buf,1);
        break;
      case 6: /* ^F: flush soundout */
        flush_soundout=sobufpos;
        break;
      case 20: /* ^T: send a test request */
        *buf='T';netout(buf,1);
        break;
      case 24: /* ^X: quit */
        *buf='Q';netout(buf,1);
        quitting=1;
        break;
      case 8:
      case KEY_BACKSPACE:
      case KEY_DC: /* Send a backspace */
        buf[0]='C';buf[1]=8;
        netout(buf,2);
        disp_writeown(8);
        break;
      case 10:
      case 13:
      case KEY_ENTER: /* Send a newline */
        buf[0]='C';buf[1]=10;
        netout(buf,2);
        disp_writeown(10);
        break;
      default: /* Send the character */
        buf[0]='C';buf[1]=ch;
        netout(buf,2);
        disp_writeown(ch);
        break;
    }
  }
}
