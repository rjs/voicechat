# Makefile for voicechat

# Set these...
# C compiler, linker, and flags
CC=gcc
CFLAGS=-Wall -O2
LD=gcc
LDFLAGS=

# System-specific libraries (-lresolv, etc.)
OTHERLIBS=

# ncurses compiler flags, linker flags, and the library; plain curses
# should work too, but you may need to modify the #includes in display.c
# and keyboard.c
NCURSESCFLAGS=-I/usr/include/ncurses
NCURSESLFLAGS=
NCURSESLIB=-lncurses

# GSM library compiler flags, linker flags, and the library
#GSMCFLAGS=-I/usr/local/include
#GSMLFLAGS=-L/usr/local/lib
GSMCFLAGS=
GSMLFLAGS=
GSMLIB=-lgsm

# No modifications should be needed below this line...

OBJS=bufin.o bufout.o display.o error.o keyboard.o net.o sound.o voicechat.o

all: voicechat voicevolume

voicechat: $(OBJS) Makefile
	$(LD) $(LDFLAGS) $(NCURSESLFLAGS) $(GSMLFLAGS) -o voicechat \
		$(OBJS) $(NCURSESLIB) $(GSMLIB) $(OTHERLIBS)

voicevolume: voicevolume.c config.h Makefile
	$(CC) $(CFLAGS) -o voicevolume.o -c voicevolume.c
	$(LD) $(LDFLAGS) -o voicevolume voicevolume.o $(OTHERLIBS)

clean:
	rm -f $(OBJS) voicevolume.o

realclean: clean
	rm -f voicechat voicevolume

bufin.o: bufin.c voicechat.h config.h Makefile

bufout.o: bufout.c voicechat.h config.h Makefile

display.o: display.c voicechat.h config.h Makefile
	$(CC) $(CFLAGS) $(NCURSESCFLAGS) -o display.o -c display.c

error.o: error.c voicechat.h config.h Makefile

keyboard.o: keyboard.c voicechat.h config.h Makefile
	$(CC) $(CFLAGS) $(NCURSESCFLAGS) -o keyboard.o -c keyboard.c

net.o: net.c voicechat.h config.h Makefile

sound.o: sound.c voicechat.h ulaw.h config.h Makefile
	$(CC) $(CFLAGS) $(GSMCFLAGS) -o sound.o -c sound.c

voicechat.o: voicechat.c voicechat.h config.h Makefile
