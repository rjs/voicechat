/*
 * voicechat -- Voice chat via TCP/IP
 * Copyright (C) 1995-1996 Riku Saikkonen
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Author: Riku Saikkonen <rjs@netti.fi>
 */

#include <stdio.h>
#ifdef SVR4
#include <curses.h>
#else
#include <ncurses.h>
#endif
#include "voicechat.h"

void disp_updatestatus(void);

int display_initialised=0;
int orecording=0,update_status=0;

char *statlntext[5]={"Rec","Play","Pause","ORec","OPause"};
WINDOW *ownwin,*otherwin,*statuswin,*msgwin;

void disp_init(void)
{
  int cols,lns,i;
  
  initscr();

  cbreak();noecho();
  nodelay(stdscr,TRUE);
  timeout(0);
  meta(stdscr,TRUE);
  keypad(stdscr,TRUE);

  getmaxyx(stdscr,lns,cols);

  i=(lns-3)/2;
  ownwin=newwin(i,cols,0,0);
  otherwin=newwin(i,cols,i,0);
  statuswin=newwin(1,cols,lns-3,0);
  msgwin=newwin(2,cols,lns-2,0);

  scrollok(ownwin,TRUE);
  wclear(ownwin);
  
  scrollok(otherwin,TRUE);
  wclear(otherwin);
  
  wclear(msgwin);

  refresh();
  display_initialised=1;
}

void disp_tick(void)
{
  if (update_status) disp_updatestatus();
  
  wrefresh(otherwin);wrefresh(statuswin);wrefresh(msgwin);wrefresh(ownwin);
}

void disp_redraw(void)
{
  clearok(curscr,TRUE);refresh();
}

void disp_deinit(void)
{
  delwin(ownwin);
  delwin(otherwin);
  delwin(statuswin);
  delwin(msgwin);
  endwin();
  display_initialised=0;
}

void disp_writeown(char ch)
{
  waddch(ownwin,ch);
}

void disp_writeother(char ch)
{
  waddch(otherwin,ch);
}

void disp_writemsg(char *msg)
{
  waddch(msgwin,'\n');
  waddstr(msgwin,msg);
}

void disp_updatestatus(void)
{
  char s[80];
  
  wmove(statuswin,0,0);
  wclrtoeol(statuswin);
  
  sprintf(s,"%3.3s %4.4s   Silence %d   Net in %dK, out %dK",
    recording?"Rec":"   ",orecording?"ORec":"    ",
    silencelimit,netinsize/1024,netoutsize/1024);
  waddstr(statuswin,s);
}
