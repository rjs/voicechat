/*
 * voicechat -- Voice chat via TCP/IP
 * Copyright (C) 1995-1996 Riku Saikkonen
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Author: Riku Saikkonen <rjs@netti.fi>
 */

#include <stdio.h>
#include <stdlib.h>
#include "voicechat.h"

void message(char *msg)
{
  if (display_initialised)
    disp_writemsg(msg);
  else
  {
    fputs(msg,stderr);
    fputc(10,stderr);
  }
}

void warning(char *msg)
{
  char s[80];
  
  sprintf(s,"Warning: %s!",msg);
  
  if (display_initialised)
    disp_writemsg(s);
  else
  {
    fputs(s,stderr);
    fputc(10,stderr);
  }
}

void fatal(char *msg)
{
  if (display_initialised) disp_deinit();

  fprintf(stderr,"Error %s!\n",msg);
  exit(1);
}

void fatals(char *msg)
{
  char s[80];

  if (display_initialised) disp_deinit();

  sprintf(s,"Error %s: ",msg);
  perror(s);
  exit(1);
}
