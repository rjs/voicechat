/*
 * voicechat -- Voice chat via TCP/IP
 * Copyright (C) 1995-1996 Riku Saikkonen
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Author: Riku Saikkonen <rjs@netti.fi>
 */

#ifndef CONFIG_H

/* Configuration */

/* Default silence limit */
#define DEF_SILENCELIMIT 2000

/* Audio devices */
#define SOUND_PLAYDEVICE "/dev/audio"
#define SOUND_RECDEVICE "/dev/audio"

/* Unidirectional sound? */
/* (VoxWare 3.x and lower needs this) */
#define SOUND_UNIDIR

/* For bidirectional sound, open SOUND_PLAYDEVICE as read/write? */
#undef SOUND_ONEDEVICE

/* If select() does not work for your audio devices, define this */
/* Most notably, VoxWare (at least 3.0.1 and older) needs this */
#define NO_SOUND_SELECT

#endif /* CONFIG_H */
