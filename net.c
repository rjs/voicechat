/*
 * voicechat -- Voice chat via TCP/IP
 * Copyright (C) 1995-1996 Riku Saikkonen
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Author: Riku Saikkonen <rjs@netti.fi>
 */

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include "voicechat.h"

int netfd=-1;

void open_net_connect(char *hostname, int port)
{
  struct sockaddr_in sin;
  struct hostent *hent;

  netfd=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
  if (netfd==-1) fatals("opening network socket");

  sin.sin_family=AF_INET;
  sin.sin_port=htons(port);

  if ((sin.sin_addr.s_addr=inet_addr(hostname))==-1) /* Not an IP number */
  {
    hent=gethostbyname(hostname);
    if (hent==NULL) fatal("resolving host name");
    memcpy(&sin.sin_addr,hent->h_addr_list[0],sizeof(sin.sin_addr));
  }

  if (connect(netfd,(struct sockaddr *)&sin,sizeof(sin))==-1)
    fatals("connecting to remote");
}

void open_net_listen(int port)
{
  struct sockaddr_in sin;
  int fd;
  socklen_t sl = sizeof(struct sockaddr_in);

  fd=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
  if (fd==-1) fatals("opening network socket");
  
  sin.sin_family=AF_INET;
  sin.sin_addr.s_addr=INADDR_ANY;
  sin.sin_port=htons(port);

  if (bind(fd,(struct sockaddr *)&sin,sizeof(sin))==-1)
    fatals("binding network socket");

  if (listen(fd,1)==-1) fatals("listening on network socket");

  if ((netfd=accept(fd,(struct sockaddr *)&sin,&sl))==-1)
    fatals("accepting a connection");

  close(fd); /* XXX Don't always accept first connection */
}

void close_net(void)
{
  if (netfd>=0) close(netfd);
}
