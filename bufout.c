/*
 * voicechat -- Voice chat via TCP/IP
 * Copyright (C) 1995-1996 Riku Saikkonen
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Author: Riku Saikkonen <rjs@netti.fi>
 */

#include <unistd.h>
#include <string.h>
#include "voicechat.h"

#define NOBUFSZ 10240 /* XXX */
#define SOBUFSZ (5*8000) /* XXX */

#define min(a,b) ((a)<(b)?(a):(b))

char nobuf[NOBUFSZ],sobuf[SOBUFSZ];
int nobufpos=0,sobufpos=0,flush_soundout=0,netoutsize=0;
 
/* Add buf to nobuf */

void netout(char *buf, int len)
{
  if (len>NOBUFSZ-nobufpos)
  {
    warning("Net output buffer overrun!");
    return;
  }

  memcpy(nobuf+nobufpos,buf,len);
  nobufpos+=len;netoutsize+=len;
}

/* netfd writable, handle it */

void handle_netout(void)
{
  int i;
  
  if (nobufpos==0) return; /* We have nothing to write */
  
  /* Write as much as we can */
  i=write(netfd,nobuf,nobufpos);
  if (i<=0) return; /* Couldn't write anything */
  
  memmove(nobuf,nobuf+i,nobufpos-i);nobufpos-=i;
}

/* Add buf to sobuf */

void soundout(char *buf, int len)
{
  if (len>SOBUFSZ-sobufpos)
  {
    warning("Sound output buffer overrun!");
    return;
  }

  memcpy(sobuf+sobufpos,buf,len);
  sobufpos+=len;
}

/* soundfd writable, handle it */

void handle_soundout(void)
{
  int i;

  if (flush_soundout==0) 
  {
    if (sobufpos>SOBUFSZ-4000)
      flush_soundout=sobufpos; /* Force flushing */
    else return; /* We have nothing to write */
  }
  
  /* Write as much as we can */
  i=write(soundfd,sobuf,flush_soundout);
  if (i<=0) return; /* Couldn't write anything */

  memmove(sobuf,sobuf+i,sobufpos-i);sobufpos-=i;flush_soundout-=i;

  if (flush_soundout==0) play_sound();
}
