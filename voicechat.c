/*
 * voicechat -- Voice chat via TCP/IP
 * Copyright (C) 1995-1996 Riku Saikkonen
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Author: Riku Saikkonen <rjs@netti.fi>
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
#include "voicechat.h"

#define max(a,b) ((a)>(b)?(a):(b))

void mainloop(void);

int quitting=0;
time_t starttm;

int main(int argc, char *argv[])
{
  fputs(SHORTBANNER "\n" BANNER "\n",stdout);

  if (argc!=3)
  {
    fputs("Syntax: voicechat hostname port\n"
      "  (hostname = \"-\": listen for a connection)\n",stderr);
    exit(1);
  }

  if (argv[1][0]=='-' && argv[1][1]==0) open_net_listen(atoi(argv[2]));
  else open_net_connect(argv[1],atoi(argv[2]));

  sound_init();
  disp_init();

  mainloop();
  
  close_net();
  sound_deinit();
  disp_deinit();

  exit(0);
}

void mainloop(void)
{
  fd_set readfds,writefds,exceptfds;
  struct timeval timeout;
  int i,maxfd;

  update_status=1;

  do
  {
    disp_tick();

    /* Select loop */

    FD_ZERO(&readfds);FD_ZERO(&writefds);FD_ZERO(&exceptfds);

    if (recording) FD_SET(soundrfd,&readfds); 
#ifdef SOUND_UNIDIR
    else 
#endif
    if (sobufpos>0) FD_SET(soundfd,&writefds);

    FD_SET(STDIN_FILENO,&readfds);
    FD_SET(netfd,&readfds);
    if (nobufpos>0) FD_SET(netfd,&writefds);
    timeout.tv_sec=30;timeout.tv_usec=0;

    maxfd=FD_SETSIZE; /* XXX Inefficient */

    i=select(maxfd,&readfds,&writefds,&exceptfds,&timeout);
    if (i<0) fatals("in select");
    if (i==0) continue;

    if (FD_ISSET(STDIN_FILENO,&readfds)) handle_kbd();
    if (FD_ISSET(netfd,&readfds)) handle_netin();

    if (recording)
#ifndef NO_SOUND_SELECT
      if (FD_ISSET(soundrfd,&readfds))
#endif
        handle_soundin();

    if (FD_ISSET(netfd,&writefds)) handle_netout();

#ifndef NO_SOUND_SELECT
    if (FD_ISSET(soundfd,&writefds))
#endif
      handle_soundout();
  } while (!quitting);
  
  /* Flush netout, soundout for five seconds */
  for (i=5;i>0;i--)
  {
    handle_netout();handle_soundout();
    if (nobufpos==0 && sobufpos==0) break;
    sleep(1);
  }
}
