/*
 * voicechat -- Voice chat via TCP/IP
 * Copyright (C) 1995-1996 Riku Saikkonen
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Author: Riku Saikkonen <rjs@netti.fi>
 */

#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include "config.h"
#include "ulaw.h"

#define PACKETSIZE 1000

/* This is crude, I know... */

int main(void)
{
  int recfd,i,j;
  char ubuf[PACKETSIZE],*up;
  short s,maxv,minv;

  printf("voicevolume\nCopyright 1995-1996 Riku Saikkonen\n\n");

  recfd=open(SOUND_RECDEVICE,O_RDONLY);
  if (recfd==-1) {printf("Error opening audio device.\n");exit(1);}

  do
  {
    j=PACKETSIZE;up=ubuf;
    do
    {
      i=read(recfd,up,j);
      if (i==-1) {printf("Error reading audio.\n");exit(1);}
      j-=i;up+=i;
    } while (j>0);
    
    /* Convert from u-law to 13-bit and get volume */
    maxv=-32700;minv=32700;
    for (i=0,up=ubuf;i<PACKETSIZE;i++,up++)
    {
      s=U2S(*up);
      if (s>maxv) maxv=s;
      if (s<minv) minv=s;
    }

    printf("Volume: %5.5d %s\n",
      maxv-minv,
      (maxv-minv)>DEF_SILENCELIMIT?"":"(silent)");
  } while(1);

  /* XXX Not reached */
  
  i=S2U(0); /* dummy to avoid a warning */
  
  printf("voicevolume finished\n");
  close(recfd);
  exit(0);
}
